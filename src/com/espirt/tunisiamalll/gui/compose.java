
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;
import com.espirt.tunisiamalll.Midlet;
import java.io.IOException;

/**
 *
 *
 * @author asus pc
 */

public class compose extends Form implements CommandListener, Runnable {

    Display display;
    Midlet mid;
    private final TextField toWhom;
    private final TextField message;

    private Alert alert;
    private final Command send, cmdBack;
    MessageConnection clientConn;
    private final Form compose;

    public compose(Midlet mid) {
        super("SMS");
        compose = new Form("Compose Message");
        toWhom = new TextField("To", "", 10, TextField.PHONENUMBER);
        message = new TextField("Message", "", 600, TextField.ANY);
        send = new Command("Send", Command.BACK, 0);
        cmdBack = new Command("Back", Command.EXIT, 0);

        this.append(toWhom);
        this.append(message);
        this.addCommand(send);
        this.addCommand(cmdBack);
        this.setCommandListener(this);
    }

    public void commandAction(Command cmd, Displayable disp) {
        if (cmd == cmdBack) {
            EnseigneSuite ps = new EnseigneSuite(utils.StaticMidlet.disp);
            ps.show();
        }
        if (cmd == send) {
            Thread th = new Thread(this);
            th.start();
        }
    }

    public void run() {
        String mno = toWhom.getString();
        String msg = message.getString();
        if (mno.equals("")) {
            alert = new Alert("Alert");
            alert.setString("Enter Mobile Number!!!");
            alert.setTimeout(2000);
            display.setCurrent(alert);
        } else {
            try {
                clientConn = (MessageConnection) Connector.open("sms://" + mno);
            } catch (IOException e) {
                alert = new Alert("Alert");
                alert.setString("Unable to connect to Station because of network problem");
                alert.setTimeout(2000);
                display.setCurrent(alert);
            }
            try {
                TextMessage textmessage = (TextMessage) clientConn.newMessage(MessageConnection.TEXT_MESSAGE);
                textmessage.setAddress("sms://" + mno);
                textmessage.setPayloadText(msg);
                clientConn.send(textmessage);
            } catch (IOException e) {
                Alert alert = new Alert("Alert", "", null, AlertType.INFO);
                alert.setTimeout(Alert.FOREVER);
                alert.setString("Unable to send");
                display.setCurrent(alert);
            }
        }
    }

}
