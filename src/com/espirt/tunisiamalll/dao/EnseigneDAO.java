/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.dao;

import com.espirt.tunisiamalll.entities.Enseigne;
import com.espirt.tunisiamalll.handler.EnseigneHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author asus pc
 */
public class EnseigneDAO {

    Enseigne[] enseigne;

    public boolean insert(Enseigne enseigne) {
        try {
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/insert.php?nom=" + enseigne.getNom_enseigne() + "&ref=" + enseigne.getRef_enseigne() + "&type=" + enseigne.getType_vetement());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Enseigne[] select() {
        try {
            EnseigneHandler enseigneHandler = new EnseigneHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/select.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, enseigneHandler);
            // display the result
            enseigne = enseigneHandler.getEnseigne();
            return enseigne;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public Enseigne[] select(int i) {
        try {
            EnseigneHandler enseigneHandler = new EnseigneHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/selectenseigne.php?id=" + i);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, enseigneHandler);
            // display the result
            enseigne = enseigneHandler.getEnseigne();
            return enseigne;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public Enseigne getEnseigne(int id) {
        //Idee idee = new Idee();
        try {
            EnseigneHandler enseigneHandler = new EnseigneHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/getenseignebyid.php?id=" + id);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, enseigneHandler);
            // display the result
            enseigne = enseigneHandler.getEnseigne();
            return enseigne[0];
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public boolean update(Enseigne enseigne) {

        try {
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/updateenseigne.php?id=" + enseigne.getId() + "&nom=" + enseigne.getNom_enseigne() + "&ref=" + enseigne.getRef_enseigne() + "&type=" + enseigne.getType_vetement());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Enseigne[] chercher(String s) {
        try {
            EnseigneHandler questionHandler = new EnseigneHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/rechercherenseigne.php?nom=" + s);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, questionHandler);
            // display the result
            enseigne = questionHandler.getEnseigne();
            return enseigne;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public void delete(int id) throws IOException {
        HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/delete.php?id=" + id);
        DataInputStream dis = new DataInputStream(hc.openDataInputStream());
    }

}
