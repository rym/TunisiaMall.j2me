/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.EnseigneDAO;
import com.espirt.tunisiamalll.entities.Enseigne;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author asus pc
 */
public class ModifiEnseigne extends Form implements CommandListener, Runnable {

    Enseigne e;
    EnseigneDAO enseignedao = new EnseigneDAO();
    TextField enseigne = new TextField("Nom de l'enseigne", enseignedao.getEnseigne(Enseigne.i).getNom_enseigne(), 50, TextField.ANY);
    TextField reference = new TextField("Reference", enseignedao.getEnseigne(Enseigne.i).getRef_enseigne(), 50, TextField.ANY);
    TextField type = new TextField("Type de vetement", enseignedao.getEnseigne(Enseigne.i).getType_vetement(), 50, TextField.ANY);

    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    public ModifiEnseigne() {
        super("Modifier Enseigne");

        append(enseigne);
        append(reference);
        append(type);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new EnseigneList(e));
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
        String streq1 = enseigne.getString();
        String streq2 = reference.getString();
        String streq3 = type.getString();
        streq1 = streq1.replace(' ', '-');
        streq2 = streq2.replace(' ', '-');
        streq3 = streq3.replace(' ', '-');

        boolean result = new EnseigneDAO().update(new Enseigne(Enseigne.i, streq1, streq2, streq3));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Enseigne modifiée avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert, new EnseigneList(e));
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Modification de l'enseigne échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }
}
