/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.dao;

import com.espirt.tunisiamalll.entities.Stock;
import com.espirt.tunisiamalll.handler.StockHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author asus pc
 */
public class StockDAO {

    Stock[] stock;

    public boolean insert(Stock stock) {
        try {
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/insertstock.php?nom=" + stock.getNom_produit() + "&type=" + stock.getType_vetement() + "&qte=" + stock.getQte() + "&prixd=" + stock.getPrix_v_d() + "&prixg=" + stock.getPrix_v_g() + "&prix=" + stock.getPrix_achat());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Stock[] select() {
        try {
            StockHandler stockHandler = new StockHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/selecte.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, stockHandler);
            // display the result
            stock = stockHandler.getStock();
            return stock;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public Stock[] select(int i) {
        try {
            StockHandler stockHandler = new StockHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/selectstock.php?id=" + i);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, stockHandler);
            // display the result
            stock = stockHandler.getStock();
            return stock;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public Stock getStock(int ii) {
        //Idee idee = new Idee();
        try {
            StockHandler stockHandler = new StockHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunsia/getstockbyid.php?id=" + ii);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, stockHandler);
            // display the result
            stock = stockHandler.getStock();
            return stock[0];
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public boolean update(Stock stock) {

        try {
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/updatestock.php?id=" + stock.getId() + "&nom=" + stock.getNom_produit() + "&type=" + stock.getType_vetement() + "&qte=" + stock.getQte() + "&prixd=" + stock.getPrix_v_d() + "&prixg=" + stock.getPrix_v_g() + "&prix=" + stock.getPrix_achat());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Stock[] chercher(String s) {
        try {
            StockHandler stockHandler = new StockHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/rechercherstock.php?nom=" + s);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, stockHandler);
            // display the result
            stock = stockHandler.getStock();
            return stock;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public void delete(int nom) throws IOException {
        HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunsia/deletestock.php?nom=" + nom);
        DataInputStream dis = new DataInputStream(hc.openDataInputStream());
    }

}
