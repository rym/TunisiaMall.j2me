/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.StockDAO;
import com.espirt.tunisiamalll.entities.Stock;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author asus pc
 */
public class StockList extends List implements CommandListener, Runnable {

    Stock s;
    Midlet mid;
    javax.microedition.lcdui.Display disp;
    Stock[] stocks = new StockDAO().select();
    Command cmdAjout = new Command("Ajouter un produit", Command.SCREEN, 0);
    Command cmdModifie = new Command("Modifier un produit", Command.SCREEN, 0);
    Command cmdChercher = new Command("Chercher un produit", Command.SCREEN, 0);
    Command supprimer = new Command("Supprimer un produit", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);
    int x = -1;

    public StockList(String title, int listType) {
        super(title, listType);
    }

    public StockList(Stock stock) {
        super("Liste Stock", List.IMPLICIT);
        this.disp = disp;
        addCommand(cmdAjout);
        addCommand(cmdModifie);
        addCommand(supprimer);
        addCommand(cmdBack);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdAjout) {
            Midlet.INSTANCE.disp.setCurrent(new AjoutStock());
        }
        if (c == cmdModifie) {
            Stock.i = stocks[getSelectedIndex()].getId();
            Midlet.INSTANCE.disp.setCurrent(new ModifiStock());
        }
        if (c == cmdChercher) {
            Stock.i = stocks[getSelectedIndex()].getId();
            Midlet.INSTANCE.disp.setCurrent(new ChercherStock(s));
        }
        if (c == supprimer) {
            if (x >= 0) {
                System.out.println(x);
                try {
                    new StockDAO().delete(stocks[x].getId());
                } catch (IOException ex) {
                }
                Midlet.INSTANCE.disp.setCurrent(new StockList(s));
            }
        }
        if (c == cmdBack) {
            StockSuite ps = new StockSuite(utils.StaticMidlet.disp);
            ps.show();
        }
    }

    public void run() {

        if (stocks.length > 0) {
            for (int i = 0; i < stocks.length; i++) {
                this.append(stocks[i].getNom_produit() + " - " + stocks[i].getType_vetement() + " - " + stocks[i].getQte() + " - " + stocks[i].getPrix_v_d() + " - " + stocks[i].getPrix_v_g() + " - " + stocks[i].getPrix_achat(), null);
            }

        }
    }

}
