/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.entities.Enseigne;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author Azuz Pc
 */
public class ListRecherche extends List implements CommandListener, Runnable {

    Enseigne e;
    Command retour = new Command("Retour ", Command.SCREEN, 0);
    Command Exit = new Command("Exit", Command.EXIT, 0);

    public ListRecherche() {
        super("Les enseignes trouvées", List.IMPLICIT);
        addCommand(Exit);
        addCommand(retour);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == retour) {
            Midlet.INSTANCE.disp.setCurrent(new EnseigneList(e));
        }
        if (c == Exit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
        Enseigne[] cins = ChercherEnseigne.enseignes;
        for (int i = 0; i < cins.length; i++) {
            append(cins[i].getNom_enseigne() + "-" + cins[i].getRef_enseigne() + "-" + cins[i].getType_vetement(), null);
        }

    }

}
