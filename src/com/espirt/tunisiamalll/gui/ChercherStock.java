/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.StockDAO;
import com.espirt.tunisiamalll.entities.Stock;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author asus pc
 */
public class ChercherStock extends Form implements CommandListener {

    javax.microedition.lcdui.Display disp;
    Stock s;
    TextField t = new TextField("type du produit", "", 50, TextField.ANY);
    Command chercher = new Command("Chercher", Command.SCREEN, 0);
    Command back = new Command("Retour", Command.EXIT, 0);
    static String t1 = "";
    static Stock[] stocks = null;

    public ChercherStock(Stock stock) {
        super("cherhcer un produit");
        addCommand(chercher);
        addCommand(back);
        setCommandListener(this);
        append(t);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == chercher) {
            t1 = t.getString();
            if (t1 != null && !(t1.equals(""))) {
                stocks = new StockDAO().chercher(t1);
            }

            Midlet.INSTANCE.disp.setCurrent(new ListRecherches());

        }
        if (c == back) {
            StockSuite ps = new StockSuite(utils.StaticMidlet.disp);
            ps.show();

        }

    }

}
