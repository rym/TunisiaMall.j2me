/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.EnseigneDAO;
import com.espirt.tunisiamalll.entities.Enseigne;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author asus pc
 */
public class ChercherEnseigne extends Form implements CommandListener {

    javax.microedition.lcdui.Display disp;
    Enseigne e;
    TextField t = new TextField("nom de l'enseigne", "", 50, TextField.ANY);
    Command chercher = new Command("Chercher", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);
    static String t1 = null;
    static Enseigne[] enseignes = null;

    public ChercherEnseigne(Enseigne enseigne) {
        super("Chercher une enseigne");
        addCommand(chercher);
        addCommand(cmdBack);
        setCommandListener(this);
        append(t);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == chercher) {
            t1 = t.getString();
            if (t1 != null && !(t1.equals(""))) {
                enseignes = new EnseigneDAO().chercher(t1);
            }

            Midlet.INSTANCE.disp.setCurrent(new ListRecherche());

        }
        if (c == cmdBack) {
            EnseigneSuite ps = new EnseigneSuite(utils.StaticMidlet.disp);
            ps.show();

        }

    }

}
