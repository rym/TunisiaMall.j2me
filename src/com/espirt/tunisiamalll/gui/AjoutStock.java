/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.StockDAO;
import com.espirt.tunisiamalll.entities.Enseigne;
import com.espirt.tunisiamalll.entities.Stock;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;


/**
 *
 * @author asus pc
 */
public class AjoutStock extends Form implements CommandListener, Runnable{
    Stock s;


    TextField tfRef = new TextField("Réference", "", 50, TextField.ANY);
    TextField Type = new TextField("Type de vetement", "", 50, TextField.ANY);
    TextField Qte = new TextField("Quantité", "", 50, TextField.ANY);
    TextField Prixd = new TextField("Prix en détail", "", 50, TextField.ANY);
    TextField Prixg = new TextField("Prix en gros", "", 50, TextField.ANY);
    TextField Prix = new TextField("Prix d'achat", "", 50, TextField.ANY);

    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    

    public AjoutStock() {
         super("Stock");

        append(tfRef);
        append(Type);
        append(Qte);
        append(Prixd);
        append(Prixg);
        append(Prix);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

  

   
    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new StockList(s));
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread((Runnable) this);
            th.start();
        }

    }

    public void run() {
         String streq1 = tfRef.getString();
        String streq2 = Type.getString();
        String streq3 = Qte.getString();
        String streq4 = Prixd.getString();
        String streq5 = Prixg.getString();
        String streq6 = Prix.getString();
        boolean result = new StockDAO().insert(new Stock( streq1, streq2, Integer.parseInt(streq3), Float.parseFloat(streq4), Float.parseFloat(streq5), Float.parseFloat(streq6)));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Enseigne ajouté avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert, new StockList(s));
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Ajout de l'enseigne échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }
    
}
