/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.handler;

import com.espirt.tunisiamalll.entities.Stock;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author asus pc
 */
public class StockHandler extends DefaultHandler {

    private Vector stockVector;

    public StockHandler() {
        stockVector = new Vector();
    }

    public Stock[] getStock() {
        Stock[] personTab = new Stock[stockVector.size()];
        stockVector.copyInto(personTab);
        return personTab;
    }

    String selectedBalise = "";
    Stock seclectedStock;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Stock")) {
            seclectedStock = new Stock();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom_produit")) {
            selectedBalise = "nom_produit";
        } else if (qName.equals("type_vetement")) {
            selectedBalise = "type_vetement";
        } else if (qName.equals("qte")) {
            selectedBalise = "qte";
        } else if (qName.equals("prix_v_d")) {
            selectedBalise = "prix_v_d";
        } else if (qName.equals("prix_v_g")) {
            selectedBalise = "prix_v_g";
        } else if (qName.equals("prix_achat")) {
            selectedBalise = "prix_achat";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Stock")) {

            stockVector.addElement(seclectedStock);
            seclectedStock = null;
        } else if (qName.equals("nom_produit")) {
            selectedBalise = "";
        } else if (qName.equals("type_vetement")) {
            selectedBalise = "";
        } else if (qName.equals("qte")) {
            selectedBalise = "";
        } else if (qName.equals("prix_v_d")) {
            selectedBalise = "";
        } else if (qName.equals("prix_v_g")) {
            selectedBalise = "";
        } else if (qName.equals("prix_achat")) {
            selectedBalise = "";
        }

    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedStock != null) {
            if (selectedBalise.equals("id")) {
                seclectedStock.setId(Integer.parseInt(new String(chars, i, i1).trim()));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom_produit")) {
                seclectedStock.setNom_produit(new String(chars, i, i1).trim());
                System.out.println(new String(chars, i, i1).trim());
            }
            if (selectedBalise.equals("type_vetement")) {
                seclectedStock.setType_vetement(new String(chars, i, i1).trim());
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("qte")) {
                seclectedStock.setQte(Integer.parseInt(new String(chars, i, i1).trim()));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prix_v_d")) {
                seclectedStock.setPrix_v_d(Float.parseFloat(new String(chars, i, i1).trim()));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prix_v_g")) {
                seclectedStock.setPrix_v_g(Float.parseFloat(new String(chars, i, i1).trim()));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prix_achat")) {
                seclectedStock.setPrix_achat(Float.parseFloat(new String(chars, i, i1).trim()));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}
