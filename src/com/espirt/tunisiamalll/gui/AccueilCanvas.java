/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.entities.Enseigne;
import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Asus
 */
public class AccueilCanvas extends Canvas implements CommandListener {

    Enseigne p;
    Displayable d;
    private Midlet mid;
    int w = getWidth();
    int h = getHeight();
    int marginCadre = 82;
    int[] posx = {(w / 2) - marginCadre, (w / 2) + 18, (w / 2) - marginCadre, (w / 2) + 18, -4, w - 28};
    int[] posy = {(h / 2) - marginCadre, (h / 2) - marginCadre, (h / 2) + 18, (h / 2) + 18, h - 46, h - 46};
    int indice = 0;
    int marginbord = 50;
    Image imgEnseigne;
    Image imgStock;
    Image imgMap;
    Image imgNext;
    Image imgExit;
    Command cmdMap = new Command("Google Map", Command.SCREEN, 0);
    Image imgBgg;

    public AccueilCanvas() {

        try {
            imgEnseigne = Image.createImage("/enseigne.png");
            imgStock = Image.createImage("/stock.png");
            imgMap = Image.createImage("/google.jpg");
            imgBgg = Image.createImage("/rym.jpg");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Midlet.INSTANCE.disp.setCurrent(this);
    }

    protected void paint(Graphics g) {

        g.setColor(255, 255, 255);
        g.fillRect(0, 0, w, h);
        g.drawImage(imgBgg, 0, 0, Graphics.TOP | Graphics.LEFT);

        g.setColor(0, 0, 0);
        g.drawImage(imgEnseigne, (w / 2) - marginbord, (h / 2) - marginbord, Graphics.VCENTER | Graphics.HCENTER);
        g.setColor(255, 255, 255);
        g.drawString("Enseigne", (w / 2) - marginbord, (h / 2), Graphics.BASELINE | Graphics.HCENTER);

        g.drawImage(imgStock, (w / 2) + marginbord, (h / 2) - marginbord, Graphics.VCENTER | Graphics.HCENTER);
        g.drawString("Stock", (w / 2) + marginbord, (h / 2), Graphics.BASELINE | Graphics.HCENTER);

        g.drawImage(imgMap, (w / 2) - marginbord, (h / 2) + marginbord, Graphics.VCENTER | Graphics.HCENTER);
        g.drawString("Trouvez Nous", (w / 2) - marginbord, (h / 2) + 100, Graphics.BASELINE | Graphics.HCENTER);

        if (indice == 4 || indice == 5) {
            g.setColor(255, 0, 0);
            g.drawArc(posx[indice], posy[indice], 32, 32, 0, 360);
        } else {
            g.setColor(255, 255, 255);
            g.drawRoundRect(posx[indice], posy[indice], 64, 64, 2, 2);
        }

    }

    protected void keyPressed(int keyCode) {
        int key = getGameAction(keyCode);
        switch (key) {
            case UP:
                if (indice == 2) {
                    indice = 0;
                } else if (indice == 3) {
                    indice = 1;
                } else if (indice == 4) {
                    indice = 2;
                } else if (indice == 5) {
                    indice = 3;
                }
                repaint();
                break;
            case DOWN:
                if (indice == 0) {
                    indice = 2;
                } else if (indice == 1) {
                    indice = 3;
                } else if (indice == 2) {
                    indice = 4;
                } else if (indice == 3) {
                    indice = 5;
                }

                repaint();
                break;
            case RIGHT:
                if (indice == 0) {
                    indice = 1;
                }
                if (indice == 2) {
                    indice = 3;
                }
                if (indice == 4) {
                    indice = 5;
                }
                repaint();
                break;
            case LEFT:
                if (indice == 1) {
                    indice = 0;
                }
                if (indice == 3) {
                    indice = 2;
                }
                if (indice == 5) {
                    indice = 4;
                }
                System.out.println("indice=" + indice);
                repaint();
                break;
            case FIRE:
                if (indice == 0) {
                    System.out.println("position 0 :");

                    EnseigneSuite ps = new EnseigneSuite(utils.StaticMidlet.disp);
                    ps.show();

                    System.out.println("liste des enseignes");

                }
                if (indice == 1) {
                    System.out.println("position 0 :");

                    StockSuite ps = new StockSuite(utils.StaticMidlet.disp);
                    ps.show();

                    System.out.println("liste des enseignes");
                }

                if (indice == 3) {

                }
                if (indice == 2) {
                    Midlet.INSTANCE.disp.setCurrent(new GoogleMapsZoomCanvas(mid, d));
////                    notification
                    //   utils.StaticMidlet.disp.setCurrent(new ListNotification("", List.IMPLICIT));
                }
                if (indice == 4) {

//                 sortir
//                    System.out.println("Deconnexion time");
//                    Session.membre = new Object();
//                    Session.membre_instance = new Membre();
//                    utils.StaticMidlet.disp.setCurrent(new Authentification());
                }

                break;
        }
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdMap) {

        }

    }
}
