/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.entities.Stock;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author Azuz Pc
 */
public class ListRecherches extends List implements CommandListener, Runnable {

    Stock s;
    Command retour = new Command("Retour ", Command.SCREEN, 0);
    Command Exit = new Command("Exit", Command.EXIT, 0);

    public ListRecherches() {

        super("Les produits trouvés", List.IMPLICIT);
        addCommand(Exit);
        addCommand(retour);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == retour) {
            Midlet.INSTANCE.disp.setCurrent(new StockList(s));
        }
        if (c == Exit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
        Stock[] cins = ChercherStock.stocks;
        for (int i = 0; i < cins.length; i++) {
            this.append(cins[i].getNom_produit() + "-" + cins[i].getType_vetement() + "-" + cins[i].getQte() + "-" + cins[i].getPrix_v_d() + "-" + cins[i].getPrix_v_g() + "-" + cins[i].getPrix_achat(), null);
        }
    }

}
