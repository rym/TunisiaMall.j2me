/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.entities;

/**
 *
 * @author asus pc
 */
public class Enseigne {

    private int id;
    private String nom_enseigne;
    private String ref_enseigne;
    private String type_vetement;
    public static int i;

    public Enseigne() {
    }

    public Enseigne(String nom_enseigne, String ref_enseigne, String type_vetement) {
        this.nom_enseigne = nom_enseigne;
        this.ref_enseigne = ref_enseigne;
        this.type_vetement = type_vetement;
    }

    public Enseigne(int id, String nom_enseigne, String ref_enseigne, String type_vetement) {
        this.id = id;
        this.nom_enseigne = nom_enseigne;
        this.ref_enseigne = ref_enseigne;
        this.type_vetement = type_vetement;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom_enseigne(String nom_enseigne) {
        this.nom_enseigne = nom_enseigne;
    }

    public void setRef_enseigne(String ref_enseigne) {
        this.ref_enseigne = ref_enseigne;
    }

    public void setType_vetement(String type_vetement) {
        this.type_vetement = type_vetement;
    }

    public int getId() {
        return id;
    }

    public String getNom_enseigne() {
        return nom_enseigne;
    }

    public String getRef_enseigne() {
        return ref_enseigne;
    }

    public String getType_vetement() {
        return type_vetement;
    }

}
