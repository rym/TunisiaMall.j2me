package com.espirt.tunisiamalll.gui;

import com.jappit.midmaps.googlemaps.GoogleMaps;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDlet;

import com.jappit.midmaps.googlemaps.GoogleMapsCoordinates;
import com.jappit.midmaps.googlemaps.GoogleMapsMarker;
import com.jappit.midmaps.googlemaps.GoogleStaticMap;
import com.jappit.midmaps.googlemaps.GoogleStaticMapHandler;

public class GoogleMapsMarkerCanvas extends GoogleMapsTestCanvas implements GoogleStaticMapHandler {

    GoogleMaps gMaps = null;

    GoogleStaticMap map = null;

    public GoogleMapsMarkerCanvas(MIDlet m, Displayable testListScreen, float positionx, float positiony) {
        super(m, testListScreen);

        gMaps = new GoogleMaps();

        map = gMaps.createMap(getWidth(), getHeight(), GoogleStaticMap.FORMAT_PNG);

        map.setHandler(this);

        map.setCenter(new GoogleMapsCoordinates(positionx, positiony));

        GoogleMapsMarker redMarker = new GoogleMapsMarker(new GoogleMapsCoordinates(36.899313, 10.207709));
        redMarker.setColor(GoogleStaticMap.COLOR_RED);
        redMarker.setSize(GoogleMapsMarker.SIZE_TINY);
        redMarker.setLabel('E');

        map.addMarker(redMarker);

        map.setZoom(14);

        map.update();
    }

    protected void paint(Graphics g) {
        map.draw(g, 0, 0, Graphics.TOP | Graphics.LEFT);
    }

    public void GoogleStaticMapUpdateError(GoogleStaticMap map, int errorCode, String errorMessage) {
        showError("map error: " + errorCode + ", " + errorMessage);
    }

    public void GoogleStaticMapUpdated(GoogleStaticMap map) {
        repaint();
    }

}
