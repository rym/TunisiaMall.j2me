/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.EnseigneDAO;
import com.espirt.tunisiamalll.entities.Enseigne;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author asus pc
 */
public class EnseigneList extends List implements CommandListener,Runnable{
    Enseigne e;
    Midlet mid;
    javax.microedition.lcdui.Display disp;
    Enseigne[] enseignes = new EnseigneDAO().select();
    Command cmdAjout = new Command("Ajouter une enseigne", Command.SCREEN, 0);
    Command cmdModifie = new Command("Modifier une enseigne", Command.SCREEN, 0);
    Command cmdChercher = new Command("Chercher une enseigne", Command.SCREEN, 0);
    Command supprimer= new Command("Supprimer une enseigne",Command.SCREEN,0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);
    int x=-1;
    
    public EnseigneList(String title, int listType) {
        super(title, listType);
    }

    public EnseigneList(Enseigne enseigne) {
        super("Liste Enseigne", List.IMPLICIT);
        this.disp = disp;
        addCommand(cmdAjout);
        addCommand(cmdModifie);
        addCommand(supprimer);
        addCommand(cmdBack);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
        
        
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdAjout) {
            Midlet.INSTANCE.disp.setCurrent(new AjoutEnseigne());
        }
         if (c == cmdModifie) {
             Enseigne.i=enseignes[getSelectedIndex()].getId();
            Midlet.INSTANCE.disp.setCurrent(new ModifiEnseigne());
        }        
           if (c == cmdChercher) {
             Enseigne.i=enseignes[getSelectedIndex()].getId();
            Midlet.INSTANCE.disp.setCurrent(new ChercherEnseigne(e));
        }
          if(c==supprimer)
          {
               
                  try {
                     new EnseigneDAO().delete(enseignes[this.getSelectedIndex()].getId());
                     System.out.println("enseigne a supprimer"+enseignes[this.getSelectedIndex()].getId());
                  } catch (IOException ex) {
                  }
                  Midlet.INSTANCE.disp.setCurrent(new EnseigneList(e));
              
          }
         if (c == cmdBack) {
           EnseigneSuite ps = new EnseigneSuite(utils.StaticMidlet.disp);
                    ps.show();
        }
    }

    public void run() {
     
        if (enseignes.length > 0) {
            for (int i = 0; i < enseignes.length; i++) {
                append(enseignes[i].getNom_enseigne()+ " - " + enseignes[i].getRef_enseigne()+ " - " + enseignes[i].getType_vetement(), null);
            }
             
        }
    }

}


