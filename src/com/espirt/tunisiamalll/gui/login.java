/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.UserDAO;
import com.espirt.tunisiamalll.entities.User;
import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author asus pc
 */
public class login extends Form implements CommandListener, Runnable {

    TextField username = new TextField("Username", "", 50, TextField.ANY);
    TextField password = new TextField("Password", "", 50, TextField.PASSWORD);

    Command cmdNext = new Command("Login", Command.SCREEN, 0);
    Command cmdBack = new Command("Exit", Command.EXIT, 0);
    Image img;

    public login() throws IOException {
        super("login");
        this.img = Image.createImage("/login.png");
        this.append(img);
        append(username);
        append(password);
        addCommand(cmdNext);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdNext) {
            Thread th = new Thread(this);
            th.start();
        }
        if (c == cmdBack) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {

        String streq1 = username.getString();
        String streq2 = password.getString();

        boolean result = new UserDAO().Login(new User(streq1, streq2));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("bienvenue");
            //User.idd = new UserDAO().getId(new User(streq1, streq2));
            Midlet.INSTANCE.disp.setCurrent(alert, new AccueilCanvas());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("erreur de connexion");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }

}
