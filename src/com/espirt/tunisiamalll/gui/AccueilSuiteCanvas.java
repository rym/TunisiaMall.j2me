/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Asus
 */
public class AccueilSuiteCanvas extends Canvas {

    Midlet mid;
    int w = getWidth();
    int h = getHeight();
    int marginCadre = 82;
    int[] posx = {(w / 2) - marginCadre, (w / 2) + 18, (w / 2) - marginCadre, (w / 2) + 18, -4, w - 28};
    int[] posy = {(h / 2) - marginCadre, (h / 2) - marginCadre, (h / 2) + 18, (h / 2) + 18, h - 46, h - 46};
    int indice = 0;
    int marginbord = 50;

    Image imgBack;
    Image imgBg;

    public AccueilSuiteCanvas() {

        try {

            imgBack = Image.createImage("Back.png");
            imgBg = Image.createImage("Graduated_Blue_Background.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    protected void paint(Graphics g) {

        g.setColor(255, 255, 255);
        g.fillRect(0, 0, w, h);
        g.drawImage(imgBg, 0, 0, Graphics.TOP | Graphics.LEFT);

        g.setColor(0, 0, 0);
        //g.drawImage(img, (w / 2) - marginbord, (h / 2) - marginbord, Graphics.VCENTER | Graphics.HCENTER);
//        g.setColor(255, 255, 255);
        g.drawString("Mes Produits", (w / 2) - marginbord, (h / 2), Graphics.BASELINE | Graphics.HCENTER);

        g.drawImage(imgBack, 12, h - 30, Graphics.VCENTER | Graphics.HCENTER);

        if (indice == 4) {
            g.setColor(255, 0, 0);
            g.drawArc(posx[indice], posy[indice], 32, 32, 0, 360);
        } else {
            g.setColor(255, 255, 255);
            g.drawRoundRect(posx[indice], posy[indice], 64, 64, 2, 2);
        }

    }

    protected void keyPressed(int keyCode) {
        int key = getGameAction(keyCode);
        switch (key) {
            case UP:
                if (indice == 2) {
                    indice = 0;
                } else if (indice == 4) {
                    indice = 2;
                } else if (indice == 3) {
                    indice = 1;
                }
                repaint();
                break;
            case DOWN:
                if (indice == 0) {
                    indice = 2;
                } else if (indice == 1) {
                    indice = 3;
                } else if (indice == 2) {
                    indice = 4;
                } else if (indice == 3) {
                    indice = 4;
                }

                repaint();
                break;
            case RIGHT:
                if (indice == 0) {
                    indice = 1;
                } else if (indice == 2) {
                    indice = 3;
                }

                repaint();
                break;
            case LEFT:
                if (indice == 1) {
                    indice = 0;
                } else if (indice == 3) {
                    indice = 2;
                }

                repaint();
                break;
            case FIRE:
                if (indice == 0) {
                    System.out.println("position 0 :");

                }
                if (indice == 1) {

                }

                if (indice == 2) {
//                    Compte
                    utils.StaticMidlet.disp.setCurrent(new AccueilCanvas());
                }

                break;
        }
    }
}
