
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Ticker;
import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.entities.Enseigne;

public class Accueil extends Form implements CommandListener {
    Enseigne e;
     private final Midlet mid;
    private final Command cmdExit;
    private final Command cmdlist;
    private final Command map;
    private final Command cmdRech;
     private final Command cmdmodifier;
     private final Command cmdajouter;
      private final Command cmdsms;
Enseigne enseigne;

    //private ImageItem imgItem;
    private final Form form = new Form("Responsable d'enseigne");
 Ticker tk = new Ticker("        TunisiaMall             TunisaMall           ");
    public Accueil(Midlet mid) {
         super("Accueil");
         this.mid = mid;
         cmdlist = new Command("Liste des enseignes", Command.SCREEN, 1);
       cmdajouter = new Command("Ajouter une enseigne", Command.SCREEN, 1);
         cmdmodifier = new Command("modifier une enseigne", Command.SCREEN, 1);
        cmdRech = new Command("Rechercher une enseigne", Command.SCREEN, 0);
          cmdsms = new Command("SMS", Command.SCREEN, 0);
        map = new Command("Trouvez Nous", Command.SCREEN, 0);
        cmdExit = new Command("Quitter", Command.EXIT, 0);

        this.setTicker(tk);
       // this.append(imgItem);
        this.addCommand(cmdlist);
        this.addCommand(cmdajouter);
        this.addCommand(cmdmodifier);
        this.addCommand(cmdRech);
        this.addCommand(cmdsms);
        this.addCommand(cmdExit);
        this.addCommand(map);
        this.setCommandListener(this);
        
       
      
    }

    public void commandAction(Command c, Displayable d) {
        if(c == cmdlist){
         

       utils.StaticMidlet.disp.setCurrent(new EnseigneList(e));
              
         } 
        if(c == cmdajouter){

            
             utils.StaticMidlet.disp.setCurrent(new AjoutEnseigne());
         } 
         if(c == cmdmodifier){
         

         utils.StaticMidlet.disp.setCurrent(new ModifiEnseigne());
         } 
         else if(c==cmdRech){
         //mid.disp.setCurrent( new GoogleMapsMarkerCanvas(mid, d, (float) 36.846612, (float) 10.195538));
        utils.StaticMidlet.disp.setCurrent( new ChercherEnseigne(e));
           // mid.disp.setCurrent( new CanvasInformation());
          
        }
//         else if(c==cmdsms){
//         //mid.disp.setCurrent( new GoogleMapsMarkerCanvas(mid, d, (float) 36.846612, (float) 10.195538));
//        utils.StaticMidlet.disp.setCurrent( new SendSms());
//           // mid.disp.setCurrent( new CanvasInformation());
//          
//        }
          else if(c==cmdsms){
         //mid.disp.setCurrent( new GoogleMapsMarkerCanvas(mid, d, (float) 36.846612, (float) 10.195538));
        utils.StaticMidlet.disp.setCurrent(new compose(mid));
           // mid.disp.setCurrent( new CanvasInformation());
          
        }
        else if(c==map){
         //mid.disp.setCurrent( new GoogleMapsMarkerCanvas(mid, d, (float) 36.846612, (float) 10.195538));
        utils.StaticMidlet.disp.setCurrent( new GoogleMapsZoomCanvas(mid, d));
           // mid.disp.setCurrent( new CanvasInformation());
          
        }
       
        
        else if (c == cmdExit) {//Exit
                         mid.destroyApp(false);
       mid.notifyDestroyed();
        }
    }

   


}
