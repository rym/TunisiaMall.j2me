/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.handler;

import com.espirt.tunisiamalll.entities.Enseigne;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author asus pc
 */
public class EnseigneHandler extends DefaultHandler {

    private Vector enseigneVector;

    public EnseigneHandler() {
        enseigneVector = new Vector();
    }

    public Enseigne[] getEnseigne() {
        Enseigne[] personTab = new Enseigne[enseigneVector.size()];
        enseigneVector.copyInto(personTab);
        return personTab;
    }

    String selectedBalise = "";
    Enseigne seclectedEnseigne;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Enseigne")) {
            seclectedEnseigne = new Enseigne();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom_enseigne")) {
            selectedBalise = "nom_enseigne";
        } else if (qName.equals("ref_enseigne")) {
            selectedBalise = "ref_enseigne";
        } else if (qName.equals("type_vetement")) {
            selectedBalise = "type_vetement";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Enseigne")) {

            enseigneVector.addElement(seclectedEnseigne);
            seclectedEnseigne = null;
        } else if (qName.equals("id")) {
            selectedBalise = "";
        } else if (qName.equals("nom_enseigne")) {
            selectedBalise = "";
        } else if (qName.equals("ref_enseigne")) {
            selectedBalise = "";
        } else if (qName.equals("type_vetement")) {
            selectedBalise = "";
        }

    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedEnseigne != null) {
            if (selectedBalise.equals("id")) {
                seclectedEnseigne.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom_enseigne")) {
                seclectedEnseigne.setNom_enseigne(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("ref_enseigne")) {
                seclectedEnseigne.setRef_enseigne(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("type_vetement")) {
                seclectedEnseigne.setType_vetement(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}
