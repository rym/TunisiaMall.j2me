/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.entities;

/**
 *
 * @author asus pc
 */
public class Stock {

    private int id;
    private String nom_produit;
    private String type_vetement;
    private int qte;
    private float prix_v_d;
    private float prix_v_g;
    private float prix_achat;
    public static int i;

    public Stock() {
    }

    public String toString() {
        return "Stock{" + "id=" + id + ", nom_produit=" + nom_produit + ", type_vetement=" + type_vetement + ", qte=" + qte + ", prix_v_d=" + prix_v_d + ", prix_v_g=" + prix_v_g + ", prix_achat=" + prix_achat + '}';
    }

    public Stock(int id, String nom_produit, String type_vetement, int qte, float prix_v_d, float prix_v_g, float prix_achat) {
        this.id = id;
        this.nom_produit = nom_produit;
        this.type_vetement = type_vetement;
        this.qte = qte;
        this.prix_v_d = prix_v_d;
        this.prix_v_g = prix_v_g;
        this.prix_achat = prix_achat;
    }

    public Stock(String nom_produit, String type_vetement, int qte, float prix_v_d, float prix_v_g, float prix_achat) {
        this.nom_produit = nom_produit;
        this.type_vetement = type_vetement;
        this.qte = qte;
        this.prix_v_d = prix_v_d;
        this.prix_v_g = prix_v_g;
        this.prix_achat = prix_achat;
    }

    public int getId() {
        return id;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public String getType_vetement() {
        return type_vetement;
    }

    public int getQte() {
        return qte;
    }

    public float getPrix_v_d() {
        return prix_v_d;
    }

    public float getPrix_v_g() {
        return prix_v_g;
    }

    public float getPrix_achat() {
        return prix_achat;
    }

    public static int getI() {
        return i;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }

    public void setType_vetement(String type_vetement) {
        this.type_vetement = type_vetement;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public void setPrix_v_d(float prix_v_d) {
        this.prix_v_d = prix_v_d;
    }

    public void setPrix_v_g(float prix_v_g) {
        this.prix_v_g = prix_v_g;
    }

    public void setPrix_achat(float prix_achat) {
        this.prix_achat = prix_achat;
    }

    public static void setI(int i) {
        Stock.i = i;
    }

}
