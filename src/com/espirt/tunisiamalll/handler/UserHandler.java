/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.handler;

import com.espirt.tunisiamalll.entities.User;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Tarek
 */
public class UserHandler extends DefaultHandler {

    private Vector userVector;

    public UserHandler() {
        userVector = new Vector();
    }

    public User[] getUser() {
        User[] users = new User[userVector.size()];
        userVector.copyInto(users);
        return users;
    }

    String selectedBalise = "";
    User seclectedUser;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("User")) {
            seclectedUser = new User();
        } else if (qName.equals("Id")) {
            selectedBalise = "Id";
        } else if (qName.equals("Username")) {
            selectedBalise = "Username";
        } else if (qName.equals("Password")) {
            selectedBalise = "Password";
        } else if (qName.equals("Email")) {
            selectedBalise = "Email";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("User")) {

            userVector.addElement(seclectedUser);
            seclectedUser = null;
        } else if (qName.equals("Id")) {
            selectedBalise = "";
        } else if (qName.equals("Username")) {
            selectedBalise = "";
        } else if (qName.equals("Password")) {
            selectedBalise = "";
        } else if (qName.equals("Email")) {
            selectedBalise = "";
        }

    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedUser != null) {
            if (selectedBalise.equals("Id")) {
                seclectedUser.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Username")) {
                seclectedUser.setUsername(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Password")) {
                seclectedUser.setPassword(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Email")) {
                seclectedUser.setEmail(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}
