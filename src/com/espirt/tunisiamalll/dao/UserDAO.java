/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.dao;

import com.espirt.tunisiamalll.entities.User;
import com.espirt.tunisiamalll.handler.UserHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Tarek
 */
public class UserDAO {

    User[] users;

    public UserDAO() {
    }

    public boolean Login(User user) {
        try {
            UserHandler userHandler = new UserHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/Tunisia/login.php?u=" + user.getUsername() + "&p=" + user.getPassword());//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, userHandler);
            // display the result
            users = userHandler.getUser();
            if (users == null) {
                return false;
            } else {
                User.idd = users[0].getId();
                return true;
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public int getId(User user) {

        try {
            UserHandler userHandler = new UserHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/getid.php?u=" + user.getUsername());//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, userHandler);
            // display the result
            users = userHandler.getUser();
            if (users == null) {
                return users[0].getId();
            } else {
                return -1;
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    public boolean Update(User user) {
        try {
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/updateprofil.php?u=" + user.getUsername() + "&p=" + user.getPassword() + "&e=" + user.getEmail() + "&id=" + User.idd);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;

    }

    public User getUser(int id) {

        try {
            UserHandler userHandler = new UserHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/getuserbyid.php?id=" + id);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, userHandler);
            // display the result
            users = userHandler.getUser();
            return users[0];
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return users[0];
    }

}
