package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;

abstract public class GoogleMapsTestCanvas1 extends Canvas implements CommandListener {

    Command back;
    Displayable testListScreen;
    MIDlet midlet;

    public GoogleMapsTestCanvas1(MIDlet m, Displayable testListScreen) {
        this.midlet = m;
        this.testListScreen = testListScreen;

        addCommand(back = new Command("back", Command.BACK, 1));

        setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == back) {
            Midlet.INSTANCE.disp.setCurrent(new AccueilCanvas());
        }

    }

    void showError(String message) {
        Alert error = new Alert("Error", message, null, AlertType.ERROR);

        Display.getDisplay(midlet).setCurrent(error, this);
    }
}
