/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.StockDAO;
import com.espirt.tunisiamalll.entities.Stock;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author asus pc
 */
public class ModifiStock extends Form implements CommandListener, Runnable {

    Stock s;
    StockDAO stockdao = new StockDAO();
    TextField reference = new TextField("Nom de l'enseigne", stockdao.getStock(Stock.i).getNom_produit(), 50, TextField.ANY);
    TextField type = new TextField("Type de vetement", stockdao.getStock(Stock.i).getType_vetement(), 50, TextField.ANY);
    TextField qte = new TextField("Quantité", Integer.toString(stockdao.getStock(Stock.i).getQte()), 50, TextField.ANY);
    TextField prixd = new TextField("prix en gros", Float.toString(stockdao.getStock(Stock.i).getPrix_v_d()), 50, TextField.ANY);
    TextField prixg = new TextField("prix en détail", Float.toString(stockdao.getStock(Stock.i).getPrix_v_g()), 50, TextField.ANY);
    TextField prix = new TextField("prix d'achat", Float.toString(stockdao.getStock(Stock.i).getPrix_achat()), 50, TextField.ANY);
    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    public ModifiStock() {
        super("Modifier produit");

        append(reference);
        append(type);
        append(qte);
        append(prixd);
        append(prixg);
        append(prix);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new StockList(s));
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
        String streq1 = reference.getString();
        String streq2 = type.getString();
        String streq3 = qte.getString();
        String streq4 = prixd.getString();
        String streq5 = prixg.getString();
        String streq6 = prix.getString();
        streq1 = streq1.replace(' ', '-');
        streq2 = streq2.replace(' ', '-');
        streq3 = streq3.replace(' ', '-');
        streq4 = streq3.replace(' ', '-');
        streq5 = streq3.replace(' ', '-');
        streq6 = streq3.replace(' ', '-');

        boolean result = new StockDAO().update(new Stock(Stock.i, streq1, streq2, Integer.parseInt(streq3), Float.parseFloat(streq4), Float.parseFloat(streq5), Float.parseFloat(streq6)));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Enseigne modifiée avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert, new StockList(s));
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Modification de l'enseigne échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }
}
