/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.dao.EnseigneDAO;
import com.espirt.tunisiamalll.entities.Enseigne;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author asus pc
 */
public class AjoutEnseigne extends Form implements CommandListener, Runnable {

    Enseigne e;

    TextField tfNom = new TextField("nom de l'enseigne", "", 50, TextField.ANY);
    TextField tfRef = new TextField("Réference", "", 50, TextField.ANY);
    TextField Type = new TextField("Type de vetement", "", 50, TextField.ANY);

    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    public AjoutEnseigne(String title) {
        super(title);
    }

    public AjoutEnseigne() {
        super("Enseigne");

        append(tfNom);
        append(tfRef);
        append(Type);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new EnseigneList(e));
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
        String strnom = tfNom.getString();
        String strref = tfRef.getString();
        String strtype = Type.getString();
        boolean result = new EnseigneDAO().insert(new Enseigne(strnom, strref, strtype));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Enseigne ajouté avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert, new EnseigneList(e));
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Ajout de l'enseigne échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }

}
