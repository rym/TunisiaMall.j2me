/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espirt.tunisiamalll.gui;

import com.espirt.tunisiamalll.Midlet;
import com.espirt.tunisiamalll.entities.Stock;
import com.sun.lwuit.Button;
import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BoxLayout;
import java.io.IOException;

/**
 *
 * @author asus pc
 */
public class StockSuite extends Form implements ActionListener {

    Stock s;
    Midlet mid;
    javax.microedition.lcdui.Display disp;
    Command exitCommand = new Command("Retour");

    public StockSuite(javax.microedition.lcdui.Display disp) {
        super("Gestion du stock");
        this.disp = disp;

        getTitleComponent().getStyle().setPadding(10, 10, 0, 0);

        getTitleStyle().setBgTransparency(127);
        getTitleStyle().setFgColor(0xffffff);
        getTitleStyle().setBgColor(0x000000);

        getMenuStyle().setBgTransparency(127);
        getMenuStyle().setFgColor(0xffffff);
        getMenuStyle().setBgColor(0x000000);

        Image img = null;
        try {
            img = Image.createImage("/Graduated_Blue_Background.png");
        } catch (IOException ex) {
        }
        setBgImage(img);

        final Button button = new Button("Consulter stock");

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                StockList cp = new StockList(s);
                StockSuite.this.disp.setCurrent(cp);
                System.out.println("consulter");
            }
        });

        final Button button2 = new Button("Rechercher un produit");

        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ChercherStock cp = new ChercherStock(s);
                StockSuite.this.disp.setCurrent(cp);
                System.out.println("rechercher");
            }
        });

        final Button button3 = new Button("Envoyer un sms");

        button3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                compose cp = new compose(mid);
                StockSuite.this.disp.setCurrent(cp);
                System.out.println("envoyer un sms");
            }
        });

        setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        button.getPressedStyle().setBorder(null);
        button.getPressedStyle().setBgTransparency(127);
        button.getPressedStyle().setMargin(5, 5, 5, 5);
        button.getPressedStyle().setPadding(20, 20, 5, 5);
        button.getPressedStyle().setFgColor(0xffffff);
        button.getPressedStyle().setMargin(40, 5, 5, 5);
        button.getPressedStyle().setPadding(10, 10, 20, 20);

        button.setAlignment(Component.CENTER);
        button.getStyle().setBorder(null);
        button.getStyle().setBgTransparency(127);
        button.getStyle().setBgColor(0x000000);
        button.getStyle().setFgColor(0xffffff);
        button.getStyle().setMargin(40, 5, 5, 5);
        button.getStyle().setPadding(10, 10, 20, 20);

        button.getSelectedStyle().setFgColor(0xffffff);
        button.getSelectedStyle().setBgTransparency(127);
        button.getSelectedStyle().setBgColor(0x000000);
        button.getSelectedStyle().setMargin(40, 5, 5, 5);
        button.getSelectedStyle().setPadding(10, 10, 20, 20);

//        button2.getPressedStyle().setBorder(null);
//        button2.setAlignment(Component.CENTER);
//        button2.getPressedStyle().setBgTransparency(127);
//        button2.getStyle().setBorder(null);
//        button2.getStyle().setBgTransparency(127);
//        button2.getStyle().setBgColor(0x000000);
//        button2.getStyle().setFgColor(0xffffff);
//        button2.getSelectedStyle().setBgTransparency(127);
//        button2.getSelectedStyle().setBgColor(0x000000);
        button2.getPressedStyle().setBorder(null);
        button2.getPressedStyle().setBgTransparency(127);
        button2.getPressedStyle().setMargin(5, 5, 5, 5);
        button2.getPressedStyle().setPadding(20, 20, 5, 5);
        button2.getPressedStyle().setFgColor(0xffffff);
        button2.getPressedStyle().setMargin(5, 5, 5, 5);
        button2.getPressedStyle().setPadding(10, 10, 20, 20);

        button2.setAlignment(Component.CENTER);
        button2.getStyle().setBorder(null);
        button2.getStyle().setBgTransparency(127);
        button2.getStyle().setBgColor(0x000000);
        button2.getStyle().setFgColor(0xffffff);
        button2.getStyle().setMargin(5, 5, 5, 5);
        button2.getStyle().setPadding(10, 10, 20, 20);

        button2.getSelectedStyle().setFgColor(0xffffff);
        button2.getSelectedStyle().setBgTransparency(127);
        button2.getSelectedStyle().setBgColor(0x000000);
        button2.getSelectedStyle().setMargin(5, 5, 5, 5);
        button2.getSelectedStyle().setPadding(10, 10, 20, 20);

        button2.getPressedStyle().setBorder(null);
        button2.getPressedStyle().setBgTransparency(127);
        button2.getPressedStyle().setMargin(5, 5, 5, 5);
        button2.getPressedStyle().setPadding(20, 20, 5, 5);
        button2.getPressedStyle().setFgColor(0xffffff);
        button2.getPressedStyle().setMargin(5, 5, 5, 5);
        button2.getPressedStyle().setPadding(10, 10, 20, 20);

        button3.setAlignment(Component.CENTER);
        button3.getStyle().setBorder(null);
        button3.getStyle().setBgTransparency(127);
        button3.getStyle().setBgColor(0x000000);
        button3.getStyle().setFgColor(0xffffff);
        button3.getStyle().setMargin(5, 5, 5, 5);
        button3.getStyle().setPadding(10, 10, 20, 20);

        button3.getSelectedStyle().setFgColor(0xffffff);
        button3.getSelectedStyle().setBgTransparency(127);
        button3.getSelectedStyle().setBgColor(0x000000);
        button3.getSelectedStyle().setMargin(5, 5, 5, 5);
        button3.getSelectedStyle().setPadding(10, 10, 20, 20);

        addComponent(0, null, button);
        addComponent(1, null, button2);
        addComponent(2, null, button3);

        show();

        addCommand(exitCommand);
        setCommandListener(this);
    }

    public void actionPerformed(ActionEvent ae) {
        disp.setCurrent(new AccueilCanvas());
    }

}
